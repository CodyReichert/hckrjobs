Rails.application.routes.draw do

  root 'home#index'

  get 'home/index'

  resources :github_jobs, only: [:index, :show]

  resources :hackernews_jobs, only: [:index, :show]

  resources :stackoverflow_jobs, only: [:index, :show]

  resources :all, only: [:index]

  resources :results, only: :index

end
