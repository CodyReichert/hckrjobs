class StackoverflowJobsController < ApplicationController

  def index
    @so_jobs = StackoverflowJob.all.paginate(page: params[:page], per_page: 25)
  end

  def show
    @so_job = StackoverflowJob.find(params[:id])
  end

end
