class AllController < ApplicationController

  def index
    @gh_jobs = GithubJob.first(15)
    @hn_jobs = HackernewsJob.first(15)
    @so_jobs = StackoverflowJob.first(15)
  end

end
