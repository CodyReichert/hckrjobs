class ResultsController < ApplicationController

  def index
    @results = PgSearch.multisearch(params[:query]).paginate(page: params[:page], per_page: 25)
    @count = @results.length
  end

end
