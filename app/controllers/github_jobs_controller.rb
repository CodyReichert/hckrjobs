class GithubJobsController < ApplicationController

  # GET /github_jobs
  # GET /github_jobs.json
  def index
    @github_jobs = GithubJob.all.paginate(page: params[:page], per_page: 25)
  end

  # GET /github_jobs/1
  # GET /github_jobs/1.json
  def show
    @github_job = GithubJob.find(params[:id])
    @company_logo = @github_job.company_logo
  end

end
