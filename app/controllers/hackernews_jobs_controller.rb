class HackernewsJobsController < ApplicationController

  def index
    @hn_jobs = HackernewsJob.all.paginate(page: params[:page], per_page: 25)
  end

  def show
    @hn_job = HackernewsJob.find(params[:id])
  end

end
