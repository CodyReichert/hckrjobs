json.array!(@github_jobs) do |github_job|
  json.extract! github_job, :id, :github_id, :created_at, :title, :location, :github_type, :description, :how_to_apply, :company, :company_url, :company_logo, :url
  json.url github_job_url(github_job, format: :json)
end
