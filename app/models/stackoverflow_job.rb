class StackoverflowJob < ActiveRecord::Base

  include PgSearch

  multisearchable :against => [:title, :description, :categories]

end
