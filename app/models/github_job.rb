class GithubJob < ActiveRecord::Base

  include PgSearch

  multisearchable :against => [:title, :description, :location]

end
