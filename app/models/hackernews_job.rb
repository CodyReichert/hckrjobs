class HackernewsJob < ActiveRecord::Base

  include PgSearch

  multisearchable :against => [:title]

end
