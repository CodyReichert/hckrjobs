class ChangeHackernewJobDateType < ActiveRecord::Migration
  def change
    change_column :hackernews_jobs, :time, :string
  end
end
