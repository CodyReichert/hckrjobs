class CreateStackoverflowJobs < ActiveRecord::Migration
  def change
    create_table :stackoverflow_jobs do |t|
      t.text :title
      t.text :link
      t.text :description
      t.text :categories
      t.timestamp :pubdate

      t.timestamps null: false
    end
    add_index :stackoverflow_jobs, :link, unique: true
  end
end
