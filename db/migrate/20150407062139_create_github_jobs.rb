class CreateGithubJobs < ActiveRecord::Migration
  def change
    create_table :github_jobs do |t|
      t.text :github_id
      t.datetime :created_at
      t.text :title
      t.text :location
      t.text :github_type
      t.text :description
      t.text :how_to_apply
      t.text :company
      t.text :company_url
      t.text :company_logo
      t.text :url

      t.timestamps null: false
    end
  end
end
