class MakeGithubIdUniqueIndex < ActiveRecord::Migration
  def change
    add_index :github_jobs, :github_id, :unique => true
  end
end
