class CreateHackernewsJobs < ActiveRecord::Migration
  def change
    create_table :hackernews_jobs do |t|
      t.integer :hn_id
      t.text :by
      t.integer :score
      t.text :text
      t.timestamp :time
      t.text :title
      t.text :hn_type
      t.text :url

      t.timestamps null: false
    end
    add_index :hackernews_jobs, :hn_id, unique: true
  end
end
