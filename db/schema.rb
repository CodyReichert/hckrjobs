# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150411214401) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "github_jobs", force: :cascade do |t|
    t.text     "github_id"
    t.datetime "created_at",   null: false
    t.text     "title"
    t.text     "location"
    t.text     "github_type"
    t.text     "description"
    t.text     "how_to_apply"
    t.text     "company"
    t.text     "company_url"
    t.text     "company_logo"
    t.text     "url"
    t.datetime "updated_at",   null: false
  end

  add_index "github_jobs", ["github_id"], name: "index_github_jobs_on_github_id", unique: true, using: :btree

  create_table "hackernews_jobs", force: :cascade do |t|
    t.integer  "hn_id"
    t.text     "by"
    t.integer  "score"
    t.text     "text"
    t.string   "time"
    t.text     "title"
    t.text     "hn_type"
    t.text     "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "hackernews_jobs", ["hn_id"], name: "index_hackernews_jobs_on_hn_id", unique: true, using: :btree

  create_table "pg_search_documents", force: :cascade do |t|
    t.text     "content"
    t.integer  "searchable_id"
    t.string   "searchable_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "stackoverflow_jobs", force: :cascade do |t|
    t.text     "title"
    t.text     "link"
    t.text     "description"
    t.text     "categories"
    t.datetime "pubdate"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "stackoverflow_jobs", ["link"], name: "index_stackoverflow_jobs_on_link", unique: true, using: :btree

end
