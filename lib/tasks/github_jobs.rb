require 'pg'
require 'json'
require 'unirest'



class Github

  def requestJobs(p)
    url = "https://jobs.github.com/positions.json?page=#{p}"
    res =  Unirest.get(url, headers: {}, parameters: nil, auth:nil)
    return res.body
  end

end



class PostgresDirect
  # Create the connection instance.
  def connect
    @conn = PG.connect(
      :dbname   => 'jobs_development',
      :user     => 'railjobs',
      :password => 'railjobs'
    )
  end


  def prepareInsertJobStatement
    query = "
             INSERT INTO github_jobs
                 (github_id, created_at, title, location, github_type,
                 description, how_to_apply, company, company_url,
                 company_logo, url, updated_at)
             SELECT
                 $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12
             WHERE NOT EXISTS
                 (SELECT 1 FROM github_jobs WHERE github_id = $1)
            "

    @conn.prepare("insert_github_job", query)
  end

  # Add a job with the prepared statement.
  def addJobs(jobs)
    jobs.each do |job|
      id           = job['id']
      created_at   = job['created_at']
      title        = job['title']
      location     = job['location']
      type         = job['type']
      desc         = job['description']
      how_to_apply = job['how_to_apply']
      company      = job['company']
      company_url  = job['company_url']
      company_logo = job['company_logo']
      url          = job['url']
      updated_at   = Time.now

      @conn.exec_prepared("insert_github_job",
                           [id, created_at, title, location, type, desc,
                            how_to_apply, company, company_url, company_logo, url, updated_at])

    end
  end

  # Print some data
  def queryJobTable
    @conn.exec( "SELECT count(*) FROM github_jobs" ) do |result|
      p result.getvalue(0, 0)
    end
  end

  def disconnect
    @conn.close
  end
end


def main
  p = PostgresDirect.new()
  p.connect
  p.prepareInsertJobStatement
  gh = Github.new
  page = 0

  loop do
    jobs = gh.requestJobs(page)
    puts "searching page #{page}"
    break if jobs.empty?
    page += 1

    begin
      p.addJobs(jobs)
      p.queryJobTable
    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
    end

  end
  ensure
    p.disconnect
end

main
