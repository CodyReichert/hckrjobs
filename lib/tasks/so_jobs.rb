require 'json'
require 'pg'
require 'rss'
require 'unirest'


class Stackoverflow

  def requestJobs
    url = 'http://careers.stackoverflow.com/jobs/feed?'
    response = Unirest.get(url)
    feed = RSS::Parser.parse response.body
    return feed.items
  end

end



class PostgresDirect

  # Create the connection instance.
  def connect
    @conn = PG.connect(
      :dbname   => 'jobs_development',
      :user     => 'railjobs',
      :password => 'railjobs'
    )
  end


  def prepareInsertJobStatement
    query = "
             INSERT INTO stackoverflow_jobs
                 (title, link, description, categories, pubdate,
                 created_at, updated_at)
             SELECT
                 $1, $2, $3, $4, $5, $6, $7
             WHERE NOT EXISTS
                 (SELECT 1 FROM stackoverflow_jobs WHERE link = $2)
            "

    @conn.prepare("insert_so_job", query)
  end

  # Add a job with the prepared statement.
  def addJobs(job)

    title       = job.title
    link        = job.link
    description = job.description
    categories  = stringCategories(job.categories)
    pubdate     = job.pubDate
    created     = Time.now
    updated     = Time.now


    @conn.exec_prepared("insert_so_job",
                        [title, link, description, categories, pubdate, created, updated]
                       )

  end


  # This is probably the worst way to do this - but it works and it still
  # searchable.
  def stringCategories(cats)
    arr = []
    cats.each do |cat|
      arr << cat.content
    end
    categories = arr.join(", ")
    return categories
  end


  # Print some data
  def queryJobTable
    @conn.exec( "SELECT count(*) FROM stackoverflow_jobs" ) do |result|
      p result.getvalue(0, 0)
    end
  end


  def disconnect
    @conn.close
  end
end


def main
  p = PostgresDirect.new()
  p.connect
  p.prepareInsertJobStatement

  so = Stackoverflow.new
  jobs = so.requestJobs

  jobs.each do |job|
    begin
      p.addJobs(job)
      p.queryJobTable
    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
    end
  end
  ensure
    p.disconnect
end

main
