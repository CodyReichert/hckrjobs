require 'pg'
require 'json'
require 'unirest'



class Hackernews

  def jobList
    url = "https://hacker-news.firebaseio.com/v0/jobstories.json"
    res =  Unirest.get(url, headers: {}, parameters: nil, auth:nil)
    return res.body
  end

  def jobDetails(id)
    url = "https://hacker-news.firebaseio.com/v0/item/#{id}.json"
    res =  Unirest.get(url, headers: {}, parameters: nil, auth:nil)
    return res.body
  end

end



class PostgresDirect

  # Create the connection instance.
  def connect
    @conn = PG.connect(
      :dbname   => 'jobs_development',
      :user     => 'railjobs',
      :password => 'railjobs'
    )
  end


  def prepareInsertJobStatement
    query = "
             INSERT INTO hackernews_jobs
                 (hn_id, by, score, text, time,
                 title, hn_type, url, created_at, updated_at)
             SELECT
                 $1, $2, $3, $4, $5, $6, $7, $8, $9, $10
             WHERE NOT EXISTS
                 (SELECT 1 FROM hackernews_jobs WHERE hn_id = $1)
            "

    @conn.prepare("insert_hn_job", query)
  end


  # Add a job with the prepared statement.
  def addJobs(job)

    id      = job['id']
    by      = job['by']
    score   = job['score']
    text    = job['text']
    time    = job['time']
    title   = job['title']
    hn_type = job['type']
    url     = job['url']
    created = Time.now
    updated = Time.now

    @conn.exec_prepared("insert_hn_job",
                        [id, by, score, text, time, title, hn_type, url, created, updated]
                       )

  end


  # Print some data
  def queryJobTable
    @conn.exec( "SELECT count(*) FROM hackernews_jobs" ) do |result|
      p result.getvalue(0, 0)
    end
  end


  def disconnect
    @conn.close
  end
end


def main
  p = PostgresDirect.new()
  p.connect
  p.prepareInsertJobStatement

  hn = Hackernews.new
  jobs = hn.jobList

  jobs.each do |job|
    j = hn.jobDetails(job)
    begin
      p.addJobs(j)
      p.queryJobTable
    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
    end
  end
  ensure
    p.disconnect
end

main
