# README

HCKRJOBS is a Rails application that aggregations job listings from the best sources
around the web (GitHub, Hacker News, and Stack Overflow). It displays them in a simple
and easily searchable manner to make finding jobs easier.

## Ruby version

Ruby `v2.2.0`, but probably works with earlier versions
Rails `v4.2.1`, but probably works with 4+.

## System dependencies

  - Ruby
  - Rails
  - Bundler

## Configuration

To run a local instance of this app, simply:

```
$ git clone git@bitbucket.org:CodyReichert/hckerjobs.git
$ cd hckerjobs/
$ bundle install
$ rake db:create
$ rake db:migrate
$ rails s
```

And in your browser navigate to `http://localost:3000`.

## How to run the test suite

```
$ rake test
```

## Services

There are three different job replicators currently: hn_jobs.rb, so_jobs.rb,
and github_jobs.rb. They currently need to be run manually, but will be integrated
into rails runner and scheduled for every 2 hours or so. To run a job replicator:

```
$ ruby lib/tasks/github_jobs.rb
$ ruby lib/tasks/so_jobs.rb
$ ruby lib/tasks/hn_jobs.rb
```
