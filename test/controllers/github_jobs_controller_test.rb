require 'test_helper'

class GithubJobsControllerTest < ActionController::TestCase
  setup do
    @github_job = github_jobs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:github_jobs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create github_job" do
    assert_difference('GithubJob.count') do
      post :create, github_job: { company: @github_job.company, company_logo: @github_job.company_logo, company_url: @github_job.company_url, created_at: @github_job.created_at, description: @github_job.description, github_id: @github_job.github_id, github_type: @github_job.github_type, how_to_apply: @github_job.how_to_apply, location: @github_job.location, title: @github_job.title, url: @github_job.url }
    end

    assert_redirected_to github_job_path(assigns(:github_job))
  end

  test "should show github_job" do
    get :show, id: @github_job
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @github_job
    assert_response :success
  end

  test "should update github_job" do
    patch :update, id: @github_job, github_job: { company: @github_job.company, company_logo: @github_job.company_logo, company_url: @github_job.company_url, created_at: @github_job.created_at, description: @github_job.description, github_id: @github_job.github_id, github_type: @github_job.github_type, how_to_apply: @github_job.how_to_apply, location: @github_job.location, title: @github_job.title, url: @github_job.url }
    assert_redirected_to github_job_path(assigns(:github_job))
  end

  test "should destroy github_job" do
    assert_difference('GithubJob.count', -1) do
      delete :destroy, id: @github_job
    end

    assert_redirected_to github_jobs_path
  end
end
